#/bin/python 
import re
import yaml
import pprint

pp = pprint.PrettyPrinter(indent=2)

raw = '''    data                        ONLINE       0     0     0
      raidz1-0                  ONLINE       0     0     0
        scsi-350014ee003b801e6  ONLINE       0     0     0
        scsi-350014ee003c65818  ONLINE       0     0     0
        scsi-350014ee2086cfbc2  ONLINE       0     0     0
        scsi-350014ee0ae7126f8  ONLINE       0     0     0
        scsi-350014ee25dc23f68  ONLINE       0     0     0'''

regex = re.compile('([\t ]+)(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)')
replaced = re.sub(regex,r'\1  "\2": \n\1    state: "\3"\n\1    read: \4\n\1    write: \5\n\1    checksum_error: \6',raw)
pp.pprint(yaml.load(replaced))
