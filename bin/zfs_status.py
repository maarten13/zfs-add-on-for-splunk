import json
import datetime
import subprocess
import re
import sys

# Import our helpers
from zfs_helpers import *

SCHEME = """<scheme>
  <title>ZFS status</title>
  <description>Get data from zpool status command.</description>
  <use_external_validation>false</use_external_validation>
  <streaming_mode>simple</streaming_mode>
  
  <endpoint>
    <args>
      <arg name="zpool_list">
        <title>Zpool List</title>
        <description>A space delimited list of one or more zpools to investigate with the zfs status command. Use ALL__POOLS to specify using all zpools returned by zpool list.</description>
        <validation>
          validate(isstr('zpool_list'),"zpool_list is not a string")
        </validation>
      </arg>
    </args>
  </endpoint>
</scheme>
"""

def do_scheme():
  print SCHEME

# Routine to get the value of an input
def get_status():
  try:
    pools_out = get_pools()    

    for pool_name in pools_out:
      status_cmd = ['sudo','zpool', 'status', pool_name]
      status_obj = subprocess.Popen(status_cmd,stdout=subprocess.PIPE).stdout.read()
      status_comp = re.compile("^\s*pool:\s+(?P<pool_o>\w+)\s+state:\s+(?P<state_o>.+?)(?:\s+status: (?P<status_o>.+?))?(?:\s+action: (?P<action_o>.+?))?(?:\s+scan: (?P<scan_o>.+))?\s+config:\s+[\r\n+](?P<config_o>.+)errors:\s+(?P<errors_o>.+)$", re.MULTILINE | re.DOTALL)
      status_vars = status_comp.search(status_obj)
      timestamp = datetime.datetime.now().isoformat()
      # Mon Sep 17 07:11:42 2018
      log_obj = status_vars.groupdict()
      print json.dumps(log_obj)

  except Exception, e:
    raise Exception, "Error getting Splunk configuration via STDIN: %s" % str(e)

# Script must implement these args: scheme, validate-arguments
if __name__ == '__main__':
  if len(sys.argv) > 1:
    if sys.argv[1] == "--scheme":
      do_scheme()
    elif sys.argv[1] == "--validate-arguments":
      validate_arguments()
    else:
      print "You giveth weird arguements"

  else:
    # dewit
    get_status()

  sys.exit(0) 
